import traceback
from rest_framework import viewsets, status
from rest_framework.response import Response
from oscarapp import models
from contactapp import models



class MoviesViewSet(viewsets.ViewSet):
    '''
    API Enpoint to access movies

    **Methods:**
        - list
        - retrieve
        - create : not allowed
        - update : not allowed
        - delete : not allowed

    **HTTP Code:**
        - 200 OK
        - 400 Bad Request
    '''

    def list(self, request):
        '''Responds with list of movies
        '''

        try:
            movies = models.Movie.objects.all().values()
            return Response({'movies': movies})
        except:
            msg = 'Error getting Movies'
            return Response({'msg': msg}, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, pk=None):
        '''Responds with details of the selected movie
        '''

        try:
            movie = models.Movie.objects.get(title=pk)
            response_hash = {
                    'id': movie.id,
                    'title': movie.title,
                    'year': movie.year,
                    'director': movie.director,
                    'actors': movie.actors,
                    'plot': movie.plot,
                    }
            return Response(response_hash)
        except:
            msg = 'Error getting Movie: {0} from database'.format(pk)
            return Response({'msg': msg}, status=status.HTTP_400_BAD_REQUEST)
