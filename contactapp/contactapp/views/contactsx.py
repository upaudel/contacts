import traceback
from rest_framework import viewsets, status
from rest_framework.response import Response
from oscarapp import models

class ContactsViewSet(viewsets.ViewSet):
   '''
    API Enpoint to access votes

    **API Endpoint**
    ::
        "http://foo.com/votes/"

    **Methods:**
        - list
        - retrieve : not allowed
        - create
        - update : not allowed
        - delete : not allowed

    **HTTP Code:**
        - 200 OK
        - 400 Bad Request
    '''

  def list(self,request):
    '''Responds with list of contacts'''      
    try:
      names = models.Contacts.objects.all()
      return Response()
    except:
      msg = 'Error getting Contacts'
      return Response({'msg':msg}, status= status.HTTP 400 BAD REQUEST)    

  def retrieve(self,request,pk=None):
      '''Responds with details of the selected movie.'''

    try:
     name = models.Contacts.objects.get(title=pk)
     response hash = {
     'first_name':contacts.firstname,
     'last_name':contacts.lastname,
     'phone':contacts.phone,
     'email':contacts.email,
     'address':contacts.address,

     }
     return Responses(response hash)
         
    except:
      msg = 'Error getting Contacts: {0} from database'.format(pk)
      return Response({'msg':msg}, status= status.HTTP 400 BAD REQUEST)


		

