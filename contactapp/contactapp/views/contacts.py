import traceback
from rest_framework import viewsets, status
from rest_framework.response import Response
from oscarapp import models
from contactapp import models

class ContactsViewSet(viewsets.ViewSet):
    '''
    API Enpoint to access contacts
    **API Endpoint**
    ::
        "http://foo.com/contacts/"
    
    **Methods:**
        - list
        - retrieve : not allowed
        - create
        - update : not allowed
        - delete : not allowed
    **HTTP Code:**
        - 200 OK
        - 400 Bad Request
    '''

    # def contact_list(self,request):
    #     contacts = {
    #         'id': contact.id,
    #         'firstname':contacts.firstname,
    #         'lastname':contacts.lastname, 
    #         'phone':contacts.phone,
    #         'email':contacts.email,
    #         'address':contacts.address,
    #         }

    #     return Response({'contacts':contacts})

    def list(self,request):
        '''Responds with list of contacts'''      
        try:
            contacts = models.Contacts.objects.all().values()
            return Response({'contacts': contacts})
        except:
            msg = 'Error getting Contacts'
            return Response({'msg':msg}, status= status.HTTP_400_BAD_REQUEST)    

    def retrieve(self,request,pk=None):
        '''Responds with details of the selected contacts.'''
        try:
            # name = models.Contacts.objects.get(email=pk)
            # contacts = name.contact_list()
            # return Responses(contacts)

            response_hash = {
            'id': contact.id,
            'firstname':contacts.firstname,
            'lastname':contacts.lastname, 
            'phone':contacts.phone,
            'email':contacts.email,
            'address':contacts.address,
            }
            return Responses(response_hash)
        except:
            msg = 'Error getting Contacts: {0} from database'.format(pk)
            return Response({'msg':msg}, status= status.HTTP_400_BAD_REQUEST) 

    def create(self, request):
        '''Adds contacts to a ContactList

        **request.data**
            - firstname
            - lastname
            - phone
            - email
            - address
        '''
        try:
            # Assumption: email (and good email) must be provided
            email = request.data.get('email')
            try:
                models.Contacts.objects.get(email=email)
                msg = 'Contact: {0} already exists'.format(email)
                return Response({'msg':msg}, status= status.HTTP_400_BAD_REQUEST) 
            except models.Contacts.DoesNotExist:
                # contact = contacts.contact_list()
                # name = models.Contacts.objects.create(**contact)
                # contact['id'] = name.id
                # return Response(contact)

                contact_data = {
                    'firstname': request.data.get('firstname'),
                    'lastname': request.data.get('lastname'),
                    'phone': request.data.get('phone'),
                    'email': email,
                    'address': request.data.get('address'),
                    }
                contact = models.Contacts.objects.create(**contact_data)
                contact_data['id'] = contact.id
                return Response(contact_data)
        except:
            raise
            msg = 'Error creating Contacts in database. Error: {0}'.format(traceback.format_exc())
            return Response({'msg': msg}, status=status.HTTP_400_BAD_REQUEST)


    def update(self, request, pk=None):
        ''' Update phone of a existing contact.'''
        try:
            new_phone = request.data.get('phone')
            contact = models.Contacts.objects.get(id=pk)
            contact.phone = new_phone
            contact.save()
            response_hash = {
                    'id': contact.id,
                    'firstname':contact.firstname,
                    'lastname':contact.lastname,
                    'phone':contact.phone,
                    'email':contact.email,
                    'address':contact.address,
                    }
            return Response({"id": contact.id})
        except:
            raise
            msg = 'Error updating Contacts in database. Error: {0}'.format(traceback.format_exc())
            return Response({'msg': msg}, status=status.HTTP_400_BAD_REQUEST)


    def destroy(self, request, pk=None):
        ''' Delete contact using email as pk.'''
        try:
            contact = models.Contacts.objects.get(email=pk)
            contact.delete()
            contact.save()
            # return None
            return Response({"id": contact.id})

            # response_hash = {
            #         'id': contact.id,
            #         'firstname':contact.firstname,
            #         'lastname':contact.lastname,
            #         'phone':contact.phone,
            #         'email':contact.email,
            #         'address':contact.address,
            #         }
            # return Response({"id": contact.id})
        except:
            raise
            msg = 'Error updating Contacts in database. Error: {0}'.format(traceback.format_exc())
            return Response({'msg': msg}, status=status.HTTP_400_BAD_REQUEST)

            



























