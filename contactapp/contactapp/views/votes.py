import traceback
from rest_framework import viewsets, status
from rest_framework.response import Response
from oscarapp import models


class VotesViewSet(viewsets.ViewSet):
    '''
    API Enpoint to access votes

    **API Endpoint**
    ::
        "http://foo.com/votes/"

    **Methods:**
        - list
        - retrieve : not allowed
        - create
        - update : not allowed
        - delete : not allowed

    **HTTP Code:**
        - 200 OK
        - 400 Bad Request
    '''

    def list(self, request):
        '''Responds with list of votes
        '''

        try:
            votes = models.VoteCount.objects.all()
            total_votes = models.VoteCount.objects.all().count()
            count_by_movie = {}
            for vote in votes:
                try:
                    count_by_movie[vote.movie.id] += 1
                except KeyError:
                    count_by_movie[vote.movie.id] = 1
            movies = models.Movie.objects.all().values()
            for movie in movies:
                try:
                    movie['total'] = count_by_movie[movie['id']]
                except KeyError:
                    movie['total'] = 0
            votes = {
                    'total': total_votes,
                    'movies': movies
                    }
            return Response({'votes': votes})
        except:
            msg = 'Error getting VoteCounts'
            return Response({'msg': msg}, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, pk=None):
        '''Responds with details of the selected vote
        '''

        try:
            try:
                movie = models.Movie.objects.get(title=pk)
            except:
                movie = models.Movie.objects.get(pk=pk)

            vote = models.VoteCount.objects.filter(movie_id=movie.id).count()
            response_hash = {
                    pk: vote
                    }
            return Response(response_hash)
        except:
            msg = 'Error getting VoteCount: {0} from database'.format(pk)
            return Response({'msg': msg}, status=status.HTTP_400_BAD_REQUEST)

    def create(self, request):
        '''Adds Vote to a movie

        **request.data**
            - movie_title or movie_id
            - age
            - gender
            - zipcode
        '''

        try:
            movie_title = request.data.get('movie_title')
            if movie_title:
                movie = models.Movie.objects.get(title=movie_title)
            else:
                movie_id = request.data.get('movie_id')
                movie = models.Movie.objects.get(pk=movie_id)
            voter_data = {
                    'age': request.data.get('age'),
                    'gender': request.data.get('gender'),
                    'zipcode': request.data.get('zipcode')
                    }
            voter = models.Voter.objects.create(**voter_data)

            # Add vote
            vote_data = {
                    'count': 1,
                    'movie_id': movie.id,
                    'voter_id': voter.id
                    }
            vote = models.VoteCount.objects.create(**vote_data)
            return Response(vote_data)
        except:
            msg = 'Error creating Vote in database. Error: {0}'.format(traceback.format_exc())
            return Response({'msg': msg}, status=status.HTTP_400_BAD_REQUEST)
